﻿using System;
using System.Collections.Generic;
using Entidades.AbarrotesPueblito;
using AccesoDatos.AbarrotesPueblito;
using System.Text.RegularExpressions; //determinar lo que debe llevar un cadena

namespace LogicaNegocio.AbarrotesPueblito
{
    public class ProveedorManejador
    {
        private ProveedorAccesoDatos _proveedorAccesodatos = new ProveedorAccesoDatos();
        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[a-z]+( [A-Z]+)*$");
            var match = regex.Match(nombre); //comprobar la cadena con la restriccion

            if (match.Success)
            {
                return true;
            }
            return false; //terminar con el metodo en caso de que no se cumpla
        }//Metodo que no se llamara por otra clase

        public Tuple<bool, string> ValidarProveedor(Proveedor proveedor)
        {
            string mensaje = "";
            bool valido = true;

            if (proveedor.Nombre.Length == 0)
            {
                mensaje = mensaje + "El nombre del Proveedor es necesario \n";
                valido = false;
            }

            else if (proveedor.Nombre.Length > 20)
            {
                mensaje = mensaje + "El nombre del Proveedor solo permite un maximo de 20 Caracteres \n";
                valido = false;
            }

            if (proveedor.Direccion.Length == 0)
            {
                mensaje = mensaje + "La direccion del Proveedor es necesaria \n";
                valido = false;
            }

            if (proveedor.Telefono.Length == 0)
            {
                mensaje = mensaje + "El telefono del Proveedor es necesario \n";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }//tuple retorna el numero de variables necesarias Multiples o almacena cualquier tipo de dato

        public void guardar(Proveedor proveedor)
        {
            _proveedorAccesodatos.guardar(proveedor);
        }
        public void eliminar(int idproveedor)
        {
            _proveedorAccesodatos.eliminar(idproveedor);
        }
        public List<Proveedor> GetProveedor(string filtro)
        {
            //List<Usuario> listusuario = new List<Usuario>();
            var listproveedor = _proveedorAccesodatos.GetProveedor(filtro);


            return listproveedor;

        }
    }
}

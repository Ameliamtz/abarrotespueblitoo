﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.AbarrotesPueblito
{
    public class Proveedor
    {
        private int _IdProveedor;
        private string _Nombre;
        private string _Direccion;
        private string _Telefono;

        public int IdProveedor { get => _IdProveedor; set => _IdProveedor = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public string Telefono { get => _Telefono; set => _Telefono = value; }
    }
}

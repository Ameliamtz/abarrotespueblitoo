create DATABASE abarrotespueblito;
use abarrotespueblito;

create table proveedores(idProveedor int  primary key auto_increment,Nombre varchar(50),Direccion varchar(50), Telefono varchar(10));

create table Productos (idProducto int primary key auto_increment,Nombre varchar(50), Precio double, fkcategoria int,
foreign key (fkcategoria) references Categoria (idCategoria) );

create table Compras (idCompra int primary key auto_increment, fkproveedor int, fkproductos int, Cantidad int, Fecha date, 
foreign key (fkproveedor)references Proveedores(idProveedor), foreign key (fkproductos) references productos(idProducto));
create table Categoria (idCategoria int primary key AUTO_INCREMENT, Nombre varchar(50));

insert into categoria values (null,'Verdura'),(null,'Refrescos'),(null,'Frutas');
insert into proveedores values(null, 'Eduardo Santos','Lagos de Moreno','4776802345'),(null,'Daniel Cruz','Leon','4741235645');
insert into productos values(null, 'Jitomate', 8.5,3);
insert into productos values(null, 'Coca-Cola', 15,1);
insert into productos values (null, 'Pi�a',20,2);
insert into compras values (null,1,2,5,'2020-02-15');
insert into compras values (null,2,1,10,'2020-02-15');

create view v_compra as
select proveedores.Nombre as Proveedor, 
Productos.Nombre as Producto, Compras.Cantidad as Cantidad, 
Compras.Fecha as Fecha
from proveedores, Productos, Compras
where Compras.fkproveedor = proveedores.idProveedor and Compras.fkproductos = Productos.idProducto;

select * from v_compra;
select Categoria.Nombre as Categoria, Productos.Nombre as Producto from Productos, Categoria where Productos.fkcategoria = Categoria.idCategoria Order by Categoria.Nombre;

create procedure insertarcompra (in id int , in prov int , in prod int, in cant int, in fecha date)
begin
select count (*) from compras where cant=Cantidad;
if cant >=1 then
insert into compras values(id,prov,prod,cant,fecha);
select "Pedido Ingresado";
else 
select "No se puede hacer el pedido";
end if;
end 

﻿using System;
using System.Collections.Generic;
using Entidades.AbarrotesPueblito;
using System.Data;

namespace AccesoDatos.AbarrotesPueblito
{
    public class ProveedorAccesoDatos
    {
        conexionAccesoDatos conexiona;

        public ProveedorAccesoDatos()
        {
            conexiona = new conexionAccesoDatos("localhost", "root", "", "abarrotespueblito", 3306);
        }

        public void guardar(Proveedor proveedor)
        {
            if (proveedor.IdProveedor == 0)
            {
                string consulta = string.Format("insert into proveedores values(null,'{0}','{1}','{2}')", proveedor.Nombre, proveedor.Direccion, proveedor.Telefono);
                conexiona.Ejecutarconsulta(consulta);
            }

            else
            {
                string consulta = string.Format("update proveedores set Nombre = '{0}', Direccion = '{1}', Telefono = '{2}' where IdProveedor = {3}", proveedor.Nombre, proveedor.Direccion, proveedor.Telefono, proveedor.IdProveedor);
                conexiona.Ejecutarconsulta(consulta);
            }
        }
        public void eliminar(int idproveedor)
        {
            string consulta = string.Format("delete from proveedores where IdProveedor = {0}", idproveedor);
            conexiona.Ejecutarconsulta(consulta);
        }
        public List<Proveedor> GetProveedor(string filtro)
        {
            //List<Usuario> listusuario = new List<Usuario>();
            var listproveedor = new List<Proveedor>();
            var ds = new DataSet();
            string consulta = "select * from proveedores where Nombre like '%" + filtro + "%'";
            ds = conexiona.Obtenerdatos(consulta, "proveedores");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow Row in dt.Rows)
            {
                var proveedor = new Proveedor
                {
                    IdProveedor = Convert.ToInt32(Row["IdProveedor"]),
                    Nombre = Row["Nombre"].ToString(),
                    Direccion = Row["Direccion"].ToString(),
                    Telefono = Row["Telefono"].ToString(),
                };
                listproveedor.Add(proveedor);
            }
            return listproveedor;
        }
    }
}

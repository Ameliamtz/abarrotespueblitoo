﻿using MySql.Data.MySqlClient;
using System.Data;

namespace AccesoDatos.AbarrotesPueblito
{
    public class conexionAccesoDatos
    {
        private MySqlConnection conn;

        public conexionAccesoDatos(string servidor, string usuario, string password, string database, uint puerto)
        {
            MySqlConnectionStringBuilder cadenaconexion = new MySqlConnectionStringBuilder();
            cadenaconexion.Server = servidor;
            cadenaconexion.UserID = usuario;
            cadenaconexion.Password = password;
            cadenaconexion.Database = database;
            cadenaconexion.Port = puerto;

            conn = new MySqlConnection(cadenaconexion.ToString());
        }
        public void Ejecutarconsulta(string consulta)
        {
            conn.Open();
            var comand = new MySqlCommand(consulta, conn);
            comand.ExecuteNonQuery();
            conn.Close();
        }

        public DataSet Obtenerdatos(string consulta, string tabla)
        {
            var ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(consulta, conn);
            da.Fill(ds, tabla);
            return ds;
        }
    }
}

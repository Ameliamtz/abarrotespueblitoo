﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.AbarrotesPueblito;
using Entidades.AbarrotesPueblito;

namespace AbarrotesPueblito
{
    public partial class FrmProveedor : Form
    {
        private ProveedorManejador _proveedormanejador;
        private Proveedor _proveedor;

        public FrmProveedor()
        {
            InitializeComponent();
            _proveedormanejador = new ProveedorManejador();
            _proveedor = new Proveedor();
        }

        private void FrmProveedor_Load(object sender, EventArgs e)
        {
            buscarproveedor("");
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }
        private void limpiarcuadros()
        {
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            lblID.Text = "0";
        }
        private void controlarcuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtTelefono.Enabled = activar;
        }

        private void controlarbotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void buscarproveedor(string filtro)
        {
            dgvProveedor.DataSource = _proveedormanejador.GetProveedor(filtro);
        }

        private bool ValidarProveedor()
        {
            var tupla = _proveedormanejador.ValidarProveedor(_proveedor);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;
            if (!valido)
            {
                MessageBox.Show(mensaje, "Error de Validacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return valido;
        }

        

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Cargarproveedor();

            if (ValidarProveedor())
            {
                guardarproveedor(); //ver definicion para ver y editar el metodo
                limpiarcuadros();
                buscarproveedor("");
                controlarbotones(true, false, false, true);
                controlarcuadros(false);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            txtNombre.Focus();
        }

        private void Cargarproveedor()
        {
            _proveedor.IdProveedor = Convert.ToInt32(lblID.Text);
            _proveedor.Nombre = txtNombre.Text;
            _proveedor.Direccion = txtDireccion.Text;
            _proveedor.Telefono = txtTelefono.Text;
        }

        private void guardarproveedor()
        {
            _proveedormanejador.guardar(_proveedor); //Utilizarlo para todo global
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta seguro que desea eliminar el registro", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    eliminarproveedor();
                    buscarproveedor("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void eliminarproveedor()
        {
            var idproveedor = dgvProveedor.CurrentRow.Cells["IdProveedor"].Value;
            _proveedormanejador.eliminar(Convert.ToInt32(idproveedor));
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarproveedor(txtBuscar.Text);
        }

        private void dgvProveedor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarProveedor();
                buscarproveedor("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void ModificarProveedor()
        {
            controlarcuadros(true);
            controlarbotones(false, true, true, false);
            lblID.Text = dgvProveedor.CurrentRow.Cells["IdProveedor"].Value.ToString();
            txtNombre.Text = dgvProveedor.CurrentRow.Cells["Nombre"].Value.ToString();
            txtDireccion.Text = dgvProveedor.CurrentRow.Cells["Direccion"].Value.ToString();
            txtTelefono.Text = dgvProveedor.CurrentRow.Cells["Telefono"].Value.ToString();
        }

    }
}
